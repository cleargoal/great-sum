<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About project great-sum

this great project makes terrific operations to sum 2 numbers with unlimited decimals...

## Installation
#### установка, развертывание
в терминале: 
1) перейти в каталог, в котором Вы создаете все свои проекты. 
2) запустить команды:

`git clone git@gitlab.com:cleargoal/double-summer.git`,

`composer install`




## Setup
#### настройка
Настроек не требуется. Проект работает на "коробочных" настройках.


## Contributing
#### участие в проекте

проект не публичный, не является open source продуктом и в нем нет необходимости  контрибьютить. 
Если Вам нужен предоставленный функционал, можете установить себе без ограничей или форкнуть.

## License

Проект основан на фреймворке Laravel, который is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
Соответственно этот проект имеет такую же лицензию.
