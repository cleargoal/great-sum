<?php


namespace App\Entities;


class TermStdClass
{

    public function __construct()
    {
        $this->name = '';
        $this->isError = false;
        $this->value = '';
        $this->hasDec = '';
        $this->hasInt = ''; // contains string part of integer part of long number
        $this->integers = []; // contains int clusters of integer part of long number
        $this->decimals = [];
        $this->ldcl = 0; // last decimal cluster Length - abbreviation - 'ldcl'
    }
}
