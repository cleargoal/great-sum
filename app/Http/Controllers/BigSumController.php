<?php

namespace App\Http\Controllers;

use App\Services\BigSumService;
use Illuminate\Http\Request;

class BigSumController extends Controller
{
    public function calculate(Request $request) {
        $termOne = $request->termOne ?? '0';
        $termTwo = $request->termTwo ?? '0';
        $getFromSumService = (new BigSumService)->sum($termOne, $termTwo);
        return response()->json($getFromSumService);
    }
}
