<?php


namespace App\Services;


use App\Entities\TermStdClass;
use Illuminate\Support\Str;

class BigSumService
{

    protected $termOne, $termTwo; // term objects of numbers that have to be added, (summands to be added - Google)
    protected $result = 'Вы не ввели числа!';
    protected $intLongSplit = 9;  // digits limit for split long integer
    protected $addToIntCluster = 0; // if junior decimal cluster has more than $this->intLongSplit digits - this value become 1

    public function __construct()
    {
        $this->termsDefinition(TermStdClass::class);
    }

    protected function termsDefinition(string $termStdClass)
    {
        $this->termOne = new $termStdClass;
        $this->termOne->name = 'Слагаемое 1';

        $this->termTwo = new $termStdClass;
        $this->termTwo->name = 'Слагаемое 2';
    }

    /**
     * Calculates SUM of two really big numbers
     *
     * @param string $termOne term 1
     * @param string $termTwo term 2
     *
     * @return string
     */
    public function sum(string $termOne, string $termTwo)
    {
        $this->termOne->value = $termOne;
        $this->termTwo->value = $termTwo;

        $this->checkTermOnError($this->termOne);
        $this->checkTermOnError($this->termTwo);
        if ($this->termOne->isError === true || $this->termTwo->isError === true) {
            return $this->result;
        }

        if ($this->checkIsAdditionAllowed()) {
            $this->result = $this->regularAddition();
        } else {
            $this->result = $this->compositedAddition();
        }

        return $this->result;
    }

    /**
     * Check is input value numeric?
     *
     * @param mixed $input input value
     *
     * @return mixed
     */
    protected function checkIsNumeric($input)
    {
        return is_numeric($input);
    }

    /**
     * Check input entity's value on allowed to be in addition operation by '+'.
     * Separate int and decimal parts of value and put them into class members
     *
     * @param string $term input string
     *
     * @return bool
     */
    protected function checkIsAdditionAllowed()
    {
        if (Str::contains($this->termOne->value, '.')) {
            $dotPosition = stripos($this->termOne->value, '.');
            $this->termOne->hasInt = Str::substr($this->termOne->value, 0, $dotPosition);
            $this->termOne->hasDec = Str::substr($this->termOne->value, $dotPosition + 1);
            $tempTermOneValue = ceil($this->termOne->value);
        } else {
            $this->termOne->hasInt = $this->termOne->value;
        }
        if (Str::contains($this->termTwo->value, '.')) {
            $dotPosition = stripos($this->termTwo->value, '.');
            $this->termTwo->hasInt = Str::substr($this->termTwo->value, 0, $dotPosition);
            $this->termTwo->hasDec = Str::substr($this->termTwo->value, $dotPosition + 1);
            $tempTermTwoValue = ceil($this->termTwo->value);
        } else {
            $this->termTwo->hasInt = $this->termTwo->value;
        }
//        return ($tempTermOneValue ?? $this->termOne->value) + ($tempTermTwoValue ?? $this->termTwo->value) <= PHP_INT_MAX;
        return ($tempTermOneValue ?? $this->termOne->value) + ($tempTermTwoValue ?? $this->termTwo->value) <= 9000000000000111; // empiric way found value :)
    }

    /**
     * Check term One on input errors
     * @param $term
     * @return void
     */
    protected function checkTermOnError($term)
    {
        if ($this->checkIsNumeric($term->value) == false) {
            $term->isError = true;
            $this->setReturnResult('Ошибка: ', $term->name, ' не является числом. ');
        } elseif (Str::startsWith($term->value, '-')) {
            $term->isError = true;
            $this->setReturnResult('Ошибка: ', $term->name, ' отрицательное. ');
        }
    }

    /**
     * Fill $this->result with term result
     * @param $status
     * @param $termName
     * @param $description
     * @return void
     */
    protected function setReturnResult($status, $termName, $description)
    {
        $this->result = $status . $termName . $description;
    }

    /**
     * Regular Addition by '+'
     *
     * @return string
     */
    protected function regularAddition()
    {
        $resultInt = intval($this->termOne->value) + intval($this->termTwo->value);
        $resultSum = $this->intToString($resultInt);

        if (!empty($this->termOne->hasDec) && !empty($this->termTwo->hasDec)) { // this IF have to be first!
            $decimals = $this->combineDecimals();
            $resultInt += $decimals[1];
            $resultSum = $this->intToString($resultInt);
            $resultSum .= '.';
            $resultSum .= $decimals[0];
        } elseif (!empty($this->termOne->hasDec) || !empty($this->termTwo->hasDec)) {
            $resultSum .= '.';
            $resultSum .= !empty($this->termOne->hasDec) ? $this->termOne->hasDec : $this->termTwo->hasDec;
        }

        return $resultSum;
    }

    /**
     * Put integer to string with everything needed
     * @param $intResult
     * @return string
     */
    protected function intToString($intResult)
    {
        return sprintf('%s', number_format($intResult, 0, '.', ' '));
    }

    /**
     * Combine Decimals due them rounded on '0' when has many clusters
     *
     * @return array
     */
    protected function combineDecimals()
    {
        $floatResult = ['', 0];
        $floatOne = $this->termOne->hasDec;
        $floatOneLength = strlen($this->termOne->hasDec);
        $floatTwo = $this->termTwo->hasDec;
        $floatTwoLength = strlen($this->termTwo->hasDec);
        if ($floatTwoLength > $floatOneLength) {
            $zeroFillOne = $floatTwoLength - $floatOneLength; // how many digits difference is
            $floatTwoInt = intval($floatTwo);
            $floatOneInt = intval($floatOne) * pow(10, $zeroFillOne);
            $floatSum = strval($floatTwoInt + $floatOneInt);
            $increment = 0;
            $floatResult = [$floatSum, $increment];

            if ($floatTwoLength < strlen($floatSum)) { // if floats addition produces next digit
                $increment = 1;
                $floatResult = [Str::of($floatSum)->substr(1), $increment];
            }
        } elseif ($floatOneLength > $floatTwoLength) {
            $zeroFillTwo = $floatOneLength - $floatTwoLength; // how many digits difference is
            $floatOneInt = intval($floatOne);
            $floatTwoInt = intval($floatTwo) * pow(10, $zeroFillTwo);
            $floatSum = strval($floatOneInt + $floatTwoInt);
            $increment = 0;
            $floatResult = [$floatSum, $increment];

            if ($floatOneLength < strlen($floatSum)) { // if floats addition produces next digit
                $increment = 1;
                $floatResult = [Str::of($floatSum)->substr(1), $increment];
            }
        }
        return $floatResult;
    }

    /**
     * Complicated composite Addition - for very long number string
     *
     * @return string
     */
    protected function compositedAddition()
    {
        $decAddition = $this->getDec();
        $intAddition = $this->getInt(); // get Int after decimals because decimals may produce int = 1

        return $intAddition . '.' . $decAddition;
    }

    /**
     * Divide integer part of number on clusters of 9 digits and put it into array
     * the property 'integers' of Term number object
     *
     * @param $term
     * @return void
     */
    protected function prepareIntClusters($term)
    {
        $hasInt = str_split(strrev($term->hasInt), $this->intLongSplit); // strrev is needed for correct splitting as RTL
        $term->integers = array_map(
            function ($element) {
                return intval(strrev($element)); // restore right integers
            },
            $hasInt
        );
    }

    /**
     * Calculate sum of integer part of given numbers
     * @param $long
     * @param $short
     * @return string
     */
    protected function longAdditionInt($long, $short)
    {
        $resultInt = ''; // string result of int part of numbers sum
        $addToAboveCluster = 0; // if previous cluster has more than $this->intLongSplit digits - this value become 1
        $lastIntCluster = count($long->integers) - 1;
        foreach ($long->integers as $key => $item) {
            $shortInt = isset($short->integers[$key]) ? $short->integers[$key] : 0; // paired decimals cluster from other number
            $valueFromDec = $key === 0 ? $this->addToIntCluster : 0; // if decimals produced int 1 it have to be added to 1-st int cluster
            $clusterSum = strval($item + $shortInt + $addToAboveCluster + $valueFromDec);

            if (strlen($clusterSum) > $this->intLongSplit) { // check - if it's need to move 1 to next/above cluster
                $addToAboveCluster = 1;
                $clusterSum = substr($clusterSum, 1); // biggest digit one moved to above cluster
            } else {
                $addToAboveCluster = 0;
            }
            if ($key < $lastIntCluster) {
                $clusterSum = str_pad($clusterSum, $this->intLongSplit, "0", STR_PAD_LEFT); // add '0'x in case if intval had delete them from original
                // integer value
            }
            $resultInt = $clusterSum . $resultInt;
        }

        return $resultInt;
    }

    /**
     * Divide decimal part of number on clusters of 9 digits and put it into array
     * the property 'decimals' of Term number object
     *
     * @param $term
     * @return void
     */
    protected function prepareDecimalClusters($term)
    {
        $hasDec = str_split($term->hasDec, $this->intLongSplit); // array
        $ldcl = 0;
        $term->decimals = array_map(
            function ($element) use (&$ldcl) {
                $ldcl = strlen($element);
                return intval($element); // restore right integers
            },
            $hasDec
        );
        $term->ldcl = $ldcl;
    }

    /**
     * Calculate sum of integer part of given numbers
     * @param $long
     * @param $short
     * @return string
     */
    protected function longAdditionDec($long, $short)
    {
        $clusterSum = [];
        $resultDec = ''; // string result of decimal part of numbers sum

        $lastDecCluster = count($long->decimals) - 1;
        foreach ($long->decimals as $key => $item) {
            $clusterSum[$key] = ($item + (isset($short->decimals[$key]) ? $short->decimals[$key] : 0));
            if (strlen(strval($clusterSum[$key])) > $this->intLongSplit) { // check - if it's need to move 1 to next/above cluster
                if ($key > 0) {
                    $clusterSum[$key - 1] = $clusterSum[$key - 1] + 1;
                    $clusterSum[$key] = intval((substr(strval($clusterSum[$key]), 1))); // biggest digit one moved to above cluster
                } else {
                    $this->addToIntCluster = 1;
                }
            }
        }

        foreach ($clusterSum as $key => $item) {
            if ($key < $lastDecCluster) {
                $resultDec .= str_pad(strval($clusterSum[$key]), $this->intLongSplit, "0", STR_PAD_LEFT);
            } else {
                $resultDec .= str_pad(strval($clusterSum[$key]), $long->ldcl, "0", STR_PAD_LEFT);
            }
        }

        return $resultDec;
    }

    protected function getInt()
    {
        $this->prepareIntClusters($this->termOne);
        $this->prepareIntClusters($this->termTwo);
        if (count($this->termOne->integers) > count($this->termTwo->integers)) {
            $long = $this->termOne;
            $short = $this->termTwo;
        } else {
            $long = $this->termTwo;
            $short = $this->termOne;
        }
        return $this->longAdditionInt($long, $short);
    }

    protected function getDec()
    {
        $this->prepareDecimalClusters($this->termOne);
        $this->prepareDecimalClusters($this->termTwo);
        if (count($this->termOne->decimals) > count($this->termTwo->decimals)) {
            $long = $this->termOne;
            $short = $this->termTwo;
        } else {
            $long = $this->termTwo;
            $short = $this->termOne;
        }
        return $this->longAdditionDec($long, $short);
    }

}
