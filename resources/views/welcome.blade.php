<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Really BIG addition</title>
    <link rel="shortcut icon" href="{{asset('img/favicon.gif')}}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Laravel addition
        </div>
        <h2 class="m-b-md">
            Сложение положительных чисел
        </h2>
        <form action="" id="sumForm" method="post" class="form-layout">
            {{csrf_field()}}
            <label for="termOne">Введите 1-е слагаемое, десятичный разделитель - только точка. Максимальная длина числа - 255 цифр</label>
            <input type="text" name="termOne" id="termOne" maxlength="255" class="input-digits" autofocus>
            <label for="termTwo">Введите 2-е слагаемое, десятичный разделитель - только точка. Максимальная длина числа - 255 цифр </label>
            <input type="text" name="termTwo" id="termTwo" maxlength="255" class="input-digits">
            <label for="termTwo">Получить результат сложения</label>
            <input type="submit" id="submitBtn" class="submitBtn">
        </form>
        <div>
            <h3>Результат сложения:</h3>
            <div id="addResult" class="addResult"></div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<script>
    const addResult = document.querySelector('#addResult');
    const submitBtn = document.querySelector('#submitBtn');
    const termOne = document.querySelector('#termOne');
    const termTwo = document.querySelector('#termTwo');
    const sumForm = document.querySelector('#sumForm');
    const axUrl = "{{route('sum.it')}}";
    sumForm.addEventListener('submit', function (e) {
        e.preventDefault();
        let termOneValue = termOne.value;
        let termTwoValue = termTwo.value;
        axios.post(axUrl, {
            termOne: termOneValue,
            termTwo: termTwoValue
        })
            .then(function (response) {
            addResult.textContent = response.data
        })
            .catch(function (error) {
                console.log(error);
            });
    });
</script>
</body>
</html>
